# MOVED TO GITHUB [quantized classifier](https://github.com/joeatbayes/quantized-classifier) #

Sorry for the inconvienience.  I am slowly migrating all my work to Github and this repo
had to be re-created due to the bitbucket dropping support for Mercurial. 


### What is this repository for? ###

A general purpose, high performance machine learning classifier.  Optimized for very large data sets.


#### Basic Test results

| Prediction Type                          | Quantized Classifier | Quant  time ms |        Tensorflow CNN | TF time ms |
| ---------------------------------------- | -------------------: | -------------: | --------------------: | ---------: |
| Classify breast cancer                   |               94.81% |             60 |                94.81% |      7,080 |
| Predict death or survival of Titanic passengers |               82.00% |             80 |                77.53% |      5,520 |
| Predict Diabetes                         |               70.58% |             80 |                65.89% |      7,330 |
| Predict Liver disorder                   |                62.5% |             70 |                67.35% |      6,160 |
| Stock SPY predict 1% rise before 1% drop. class=1 (success) only | 65.9%  at 24% recall |            390 | 71.43%  at 13% recall |     59,000 |

> > * Tests measure Precision at 100% recall. Recall forced to 100% by choosing highest Prob Class from answer as the class chosen.
> > * Tensorflow n_epoch = 30, batch=55.   Tensorflow CUDA = GeForce GTX 960M.  QuantProb is only using system CPU and uses no optimizer.
> > * Run includes both train and test stage. Timing from cygwin time command
> > * All source and data to duplicate test is in repository

Please send me data sets you would like to add  to the test.


### Who do I talk to? ###

[Joseph Ellsworth](https://www.linkedin.com/in/joe-ellsworth-68222/)
